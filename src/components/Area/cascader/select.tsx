import React from 'react';
import styles from './select.module.scss';
type Option = {
  value: string;
  label: string;
};
type SelectProps = {
  options: Option[];
  value?: any;
  onChange :any;
};
const Select: React.FC<SelectProps> = ({ options, value, onChange }) => {
  const handleSelect = (option: Option) => {
    onChange(option);
  };

  
  return (
    <div className={styles['select']}>
        <div className={styles["select__options"]}>
          {options.map((option) => (
            <div
              key={option.value}
              className={`${styles['select__option']} ${value && value.value === option.value ? styles['selected'] : ''}`}
              onClick={() => handleSelect(option)}
            >
              {
              option.label
              }
            </div>
          ))}
        </div>
    </div>
  );
};
export default Select;