import React, { useState } from 'react';
import styles from './index.module.scss';
import Select from './select';
type Option = {
  value: string;
  label: string;
  children?: Option[];
};
type CascaderProps = {
  options: Option[];
  value: string[];
  onSelect: (option: Option, level: number) => void;
  level: number;
};

const Cascader: React.FC<CascaderProps> = ({ options, value, onSelect, level }) => {
  const [subOptions, setSubOptions] = useState<Option[]>([]);
  const handleChange = (opt:any) => {
    const option = opt;
    if (!option) {
      setSubOptions([]);
      return;
    }
    onSelect(option, level);
    setSubOptions(option.children || []);
  };
  
  return (
    <div className={styles['cascader-list-item']}>
      <Select options={options} value={value} onChange={handleChange} />
      {subOptions.length > 0 && (
        <Cascader
          options={subOptions}
          value={value}
          onSelect={onSelect}
          level={level + 1}
        />
      )}
    </div>
  );
};
type CascaderWrapperProps = {
  options: Option[];
};
const CascaderWrapper: React.FC<CascaderWrapperProps> = ({ options }) => {
  const [value, setValue] = useState<any>([]);
  const [menu, setMenu] = useState<any>([]);
  const [open, setOpen] = useState<boolean>(false);
  function uuid() {
    var temp_url = URL.createObjectURL(new Blob());
    var uuid = temp_url.toString(); 
    URL.revokeObjectURL(temp_url);
    return uuid.substr(uuid.lastIndexOf("/") + 1);
  }
  const handleSelect = (option: any, level: number) => {
    if(!option.children){
      setOpen((v) => !v)
    }
    option.level=level;
    setMenu(option)
    console.log(option);
    if(level==0){
      value[0]=option
    }else if(level==1){
      value[1]=option
    }else{
      value[2]=option
    }
    setValue(value);
  };
  const setOpens = () => {
    setValue([]);
    setOpen((v) => !v)
  };
  return (
    <div>
      <div className={styles['cascader-input']} onClick={setOpens} placeholder="请选择区域" contentEditable suppressContentEditableWarning>    
      {
       value.map((v:any, i:any) => (<span key={uuid()}>{v.value} / </span>))
      }  
      </div>
      {open ? (
      <div className={styles['cascader-list']}>
        <Cascader options={options} value={menu} onSelect={handleSelect} level={0} />
      </div>
        ):''}
    </div>
  );
};
export default CascaderWrapper;