import React, { useState } from 'react';
import  Cascader  from './cascader';
// import  {Cascader}  from 'antd';
interface Province {
  value: string;
  label: string;
  children: City[];
}
interface City {
  value: string;
  label: string;
  children: District[];
}
interface District {
  value: string;
  label: string;
}
const options: Province[] = [
  {
    value: '北京市',
    label: '北京市',
    children: [
      {
        value: '北京市',
        label: '北京市',
        children: [
          {
            value: '东城区',
            label: '东城区',
          },
          {
            value: '西城区',
            label: '西城区',
          },
          {
            value: '朝阳区',
            label: '朝阳区',
          },
          // 其他区县...
        ],

      },
      
    ],
  },
  {
    value: '上海市',
    label: '上海市',
    children: [
      {
        value: '上海市',
        label: '上海市',
        children: [
          {
            value: '黄浦区',
            label: '黄浦区',
          },
          {
            value: '徐汇区',
            label: '徐汇区',
          },
          {
            value: '长宁区',
            label: '长宁区',
          },
          // 其他区县...
        ],
      },
    ],
  },
  // 其他省市...
];
function App() {
  const [value, setValue] = useState<string[]>([]);
  
  function onChange(value: string[]) {
    setValue(value);
  }
  return (
    <Cascader
      options={options}
    //   onChange={onChange}
    //   value={value}
    //   placeholder="请选择省市区"
    />
  );
}
export default App;